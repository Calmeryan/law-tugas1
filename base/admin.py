from django.contrib import admin
from base.models import User

# Register your models here.
class HomeUser(admin.ModelAdmin):
    list_display = (
        'username', 
        'password', 
        'client_id', 
        "client_secret", 
        "token", 
        "refresh_token", 
        "token_created", 
        "fullname", 
        "npm"
        )

admin.site.register(User, HomeUser)