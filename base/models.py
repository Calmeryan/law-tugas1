from django.db import models
from django.forms import PasswordInput

# Create your models here.
class User(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    client_id = models.CharField(max_length= 50)
    client_secret = models.CharField(max_length= 50)
    token = models.CharField(max_length=40, null = True, blank= True)
    refresh_token = models.CharField(max_length=40, null = True, blank= True)
    token_created = models.DateTimeField(null = True, blank= True)
    fullname = models.CharField(max_length=50, null = True)
    npm = models.CharField(max_length=50, null = True)

    def __str__(self):
        return self.username
